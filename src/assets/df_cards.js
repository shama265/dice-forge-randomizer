export default {
  "name": "Cards",
  "component" : [
    {
      "name": "Hammer",
      "label": "hammer",
      "position": 0,
      "class": "intro"
    },
    {
      "name": "Chest",
      "label": "chest",
      "position": 1,
      "class": "intro"
    },
    {
      "name": "Silver Hind",
      "label": "silver_hind",
      "position": 2,
      "class": "intro"
    },
    {
      "name": "Satyrs",
      "label": "satyrs",
      "position": 3,
      "class": "intro"
    },
    {
      "name": "Ferryman",
      "label": "ferryman",
      "position": 4,
      "class": "intro"
    },
    {
      "name": "Helmet",
      "label": "helmet",
      "position": 5,
      "class": "intro"
    },
    {
      "name": "Cancer",
      "label": "cancer",
      "position": 6,
      "class": "intro"
    },
    {
      "name": "Elder",
      "label": "elder",
      "position": 7,
      "class": "intro"
    },
    {
      "name": "Spirits",
      "label": "spirits",
      "position": 8,
      "class": "intro"
    },
    {
      "name": "Owl",
      "label": "owl",
      "position": 9,
      "class": "intro"
    },
    {
      "name": "Minotaur",
      "label": "minotaur",
      "position": 10,
      "class": "intro"
    },
    {
      "name": "Gorgon",
      "label": "gorgon",
      "position": 11,
      "class": "intro"
    },
    {
      "name": "Mirror",
      "label": "mirror",
      "position": 12,
      "class": "intro"
    },
    {
      "name": "Sphinx",
      "label": "sphinx",
      "position": 13,
      "class": "intro"
    },
    {
      "name": "Hydra",
      "label": "hydra",
      "position": 14,
      "class": "intro"
    },
    {
      "name": "Great Bear",
      "label": "great_bear",
      "position": 2,
      "class": "basic"
    },
    {
      "name": "Tenacious Boar",
      "label": "tenacious_boar",
      "position": 3,
      "class": "basic"
    },
    {
      "name": "Cerberus",
      "label": "cerberus",
      "position": 4,
      "class": "basic"
    },
    {
      "name": "Sentinel",
      "label": "sentinel",
      "position": 6,
      "class": "basic"
    },
    {
      "name": "Ship",
      "label": "ship",
      "position": 9,
      "class": "basic"
    },
    {
      "name": "Shield",
      "label": "shield",
      "position": 10,
      "class": "basic"
    },
    {
      "name": "Triton",
      "label": "triton",
      "position": 11,
      "class": "basic"
    },
    {
      "name": "Cyclops",
      "label": "cyclops",
      "position": 13,
      "class": "basic"
    },
    {
      "name": "Typhon",
      "label": "typhon",
      "position": 14,
      "class": "basic"
    },
    {
      "name": "Nymph",
      "label": "nymph",
      "position": 7,
      "class": "promo"
    },
    {
      "name": "Seagull",
      "label": "seagull",
      "position": 3,
      "class": "promo"
    },
    {
      "name": "Twins",
      "label": "twins",
      "position": 0,
      "class": "goddess"
    },
    {
      "name": "Companion",
      "label": "companion",
      "position": 1,
      "class": "goddess"
    },
    {
      "name": "Moon Golem",
      "label": "moon_golem",
      "position": 2,
      "class": "goddess",
      "reserved": true
    },
    {
      "name": "Celestial Die",
      "label": "celestial_die",
      "position": 3,
      "class": "goddess"
    },
    {
      "name": "The Mists",
      "label": "the_mists",
      "position": 4,
      "class": "goddess"
    },
    {
      "name": "Giant Golem",
      "label": "giant_golem",
      "position": 5,
      "class": "goddess",
      "reserved": true
    },
    {
      "name": "Eternal Night",
      "label": "eternal_night",
      "position": 6,
      "class": "goddess"
    },
    {
      "name": "The Tree",
      "label": "the_tree",
      "position": 7,
      "class": "goddess"
    },
    {
      "name": "Wood Nymph",
      "label": "wood_nymph",
      "position": 8,
      "class": "goddess"
    },
    {
      "name": "Sun Golem",
      "label": "sun_golem",
      "position": 9,
      "class": "goddess",
      "reserved": true
    },
    {
      "name": "Time Golem",
      "label": "time_golem",
      "position": 10,
      "class": "goddess",
      "reserved": true
    },
    {
      "name": "Goldsmith",
      "label": "goldsmith",
      "position": 11,
      "class": "goddess"
    },
    {
      "name": "Trident",
      "label": "trident",
      "position": 12,
      "class": "goddess"
    },
    {
      "name": "Eternal Flame",
      "label": "eternal_flame",
      "position": 13,
      "class": "goddess"
    },
    {
      "name": "Goddess",
      "label": "goddess",
      "position": 14,
      "class": "goddess"
    },
    {
      "name": "Scepter",
      "label": "scepter",
      "position": 0,
      "class": "titan"
    },
    {
      "name": "The Memory",
      "label": "the_memory",
      "position": 1,
      "class": "titan",
      "reserved": true
    },
    {
      "name": "The Oracle",
      "label": "the_oracle",
      "position": 2,
      "class": "titan",
      "reserved": true
    },
    {
      "name": "The Wind",
      "label": "the_wind",
      "position": 3,
      "class": "titan"
    },
    {
      "name": "The Ancestor",
      "label": "the_ancestor",
      "position": 4,
      "class": "titan"
    },
    {
      "name": "The Chaos",
      "label": "the_chaos",
      "position": 5,
      "class": "titan",
      "reserved": true
    },
    {
      "name": "The Right Hand",
      "label": "the_right_hand",
      "position": 6,
      "class": "titan"
    },
    {
      "name": "Trader",
      "label": "trader",
      "position": 7,
      "class": "titan"
    },
    {
      "name": "The Stubborn",
      "label": "the_stubborn",
      "position": 8,
      "class": "titan",
      "reserved": true
    },
    {
      "name": "The Guardian",
      "label": "the_guardian",
      "position": 9,
      "class": "titan",
      "reserved": true
    },
    {
      "name": "The Light",
      "label": "the_light",
      "position": 10,
      "class": "titan"
    },
    {
      "name": "The Omniscient",
      "label": "the_omniscient",
      "position": 11,
      "class": "titan"
    },
    {
      "name": "Misfortune Mirror",
      "label": "misfortune_mirror",
      "position": 12,
      "class": "titan",
      "reserved": true
    },
    {
      "name": "The Left Hand",
      "label": "the_left_hand",
      "position": 13,
      "class": "titan"
    },
    {
      "name": "Prime Titan",
      "label": "prime_titan",
      "position": 14,
      "class": "titan"
    },
    {
      "name": "Fortune Wheel",
      "label": "fortune_wheel",
      "position": 11,
      "class": "promo"
    },
    {
      "name": "Pegasus",
      "label": "pegasus",
      "position": 10,
      "class": "promo"
    }
  ]
}
